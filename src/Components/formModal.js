import React from "react"
import "../Style/Form.css"
import "./../App.css"
import Info from "./../Images/info-24.png"
class FormBox extends React.Component {

    constructor() {
        super()
        this.state = {
            isDesktop: "",
            fileName: "",
        }
    }
    componentDidMount() {
        window.addEventListener('resize', () => {
            this.setState({
                isDesktop: window.innerWidth > 768
            });
        }, false);


    }
    getFile = e => {
        const file = e.target.files[0]
        const reader = new FileReader()
        reader.readAsText(file);
        reader.onload = () => {
            this.setState({ fileName: file.name })
        }
        reader.onerror = () => {
            console.log("file error", reader.error);
        }
    }
    render() {

        return (
            <div>

                <h1 className="heading">{this.props.heading}</h1>

                <div className="boxContainer">
                    <div className="propertyFields">
                        <div>
                            <label className="fieldLabel">Property Name <span><img src={Info} className="infoIcon" /></span></label>
                            <input type="text" placeholder="Property Name" className="fieldWidth" />
                        </div>
                        <div>
                            <label className="fieldLabel">Property Type</label>
                            <select className="fieldWidth fieldSelect colorMuted">
                                <option className="colorMuted">Property Type</option>
                                <option>Home</option>
                                <option>Land</option>
                                <option>Gutown</option>
                            </select>

                        </div>
                        <div>
                            <label className="fieldLabel">Number of Units</label>
                            <select className="fieldWidth fieldSelect">
                                <option className="colorMuted" > Number of Units</option>
                                <option>Home</option>
                                <option>Land</option>
                                <option>Gutown</option>
                            </select>

                        </div>

                    </div>

                    <div className="textareaField">

                        <label className="fieldLabel">Property Address</label>
                        <textarea placeholder="Enter Borrower Name" className="textArea colorMuted">
                        </textarea>
                    </div>
                    <div>
                        <label className="fieldLabel">File Attachment</label>
                        <div className={` uploadFilesContainer `}>
                            {this.state.fileName && <span className="fileName">{this.state.fileName}</span>}
                            <input type="file" className="defaultFile" onChange={this.getFile} />
                            <div className="fileTextAbsolute">

                                <span className="browseFileText">Browse file</span>
                                <span className="dropZoneMobile"> or
                                    {this.state.isDesktop && <span className="dropZoneDesktop"> Drag & Drop  </span>} to Attach a file</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        )
    }
}

export default FormBox 
