import React, { useState } from 'react'
import Arrow from "./../Images/arrow-48.png"
import "./../Style/Home.css"

const Layout = ({ children }) => {
    const [toggle, setToggle] = useState()
    // const toggleAction = () => {
    //     setToggle(!toggle)
    // }
    return (
        <div>
            <nav >
                <div className='mobileToggleBar'>
                    <button className="navbarToggle" onClick={() => setToggle(!toggle)}>
                        <span className="navbarToggleBar">
                        </span>
                        <span className="navbarToggleBar">
                        </span>
                        <span className="navbarToggleBar">
                        </span>
                    </button>
                </div>

                <div className={`${toggle ? "homeActive" : ""} homeNav`}>


                    <div className='brand'>
                        <span className='brandLogo'>
                            CB
                        </span>
                        <span className='brandName'>
                            Cloudbankin
                        </span>

                    </div>
                    <div className='dropDown'>
                        <span className='dropDownLogo'>
                            GC
                        </span>
                        <div className='dropDownBlock'> <span className='dropDownMenu'>Gregory Clark <img src={Arrow} className="downArrow" /></span>
                            <ul className='dropDownNone'>
                                <li>home</li>
                                <li>about</li>
                            </ul>
                        </div>


                    </div>

                </div>


            </nav>
            <div> {children}</div>
        </div>
    )
}
export default Layout;