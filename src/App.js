import React from "react"
import Layout from "./Components/Layout"
import "./App.css"
import "./Style/Form.css"
import "./Style/responsive.css"
import FormBox from "./Components/formModal"
class Home extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      currentPageIndex: 0,
    };
    this.myRef = React.createRef();

  };

  //condition to render function 
  // nextPage() {
  //   if (this.state.currentPageIndex < 4) {
  //     this.setState(prevState => {
  //       return {
  //         currentPageIndex: prevState.currentPageIndex + 1
  //       }
  //     });
  //   }
  // }


  // previousPage() {
  //   if (this.state.currentPageIndex > 0) {
  //     this.setState(prevState => {
  //       return {
  //         currentPageIndex: prevState.currentPageIndex - 1
  //       }
  //     });
  //   }
  // }

  //condition to disabled button
  nextPage() {
    this.setState(prevState => {
      return {
        currentPageIndex: prevState.currentPageIndex + 1
      }
    });
    this.myRef.current?.scrollIntoView({ behavior: "smooth", block: "start" })
  }


  previousPage() {
    this.setState(prevState => {
      return {
        currentPageIndex: prevState.currentPageIndex - 1
      }
    });
    this.myRef.current?.scrollIntoView({ behavior: 'smooth', block: "start", })
  }


  render() {
    console.log(this.state.currentPageIndex)
    const fieldTotalId = ["Borrower Company Info", "Director Info", "Financial Info", "Past Performance Details", "Document Upload"]
    return (
      <Layout home="homeStyle">
        {
          <div className="container paddingTop">

            <div className="progress" >
              {fieldTotalId.map((data, index) => {
                const list = (
                  <div key={index} ref={this.myRef} className={`${index == 0 && (this.state.currentPageIndex == 0 ? "First" : "")} ${index == 1 && (this.state.currentPageIndex == 1 ? "First " : "")} ${index == 2 && (this.state.currentPageIndex == 2 ? "First" : "")} ${index == 3 && (this.state.currentPageIndex == 3 ? "First " : "")} ${index == 4 && (this.state.currentPageIndex == 4 ? "First" : "")} progressParent`}>
                    <span className="badgeIndex">{index + 1}</span>
                    <span className="emptyDot"></span>
                    <span className="emptyDot"></span>
                    <span className="emptyDot"></span>
                    <span className="emptyDot"></span>
                    <span className="emptyDot"></span>
                    <span className="emptyDot"></span>
                    <span className="badgeData">{data}</span>
                  </div>

                );
                return list;
              })}
            </div>
            <div className="formBox">
              {this.state.currentPageIndex == 0 && <FormBox heading="Borrower Company Info" />}
              {this.state.currentPageIndex == 1 && <FormBox heading="Director Info" />}
              {this.state.currentPageIndex == 2 && <FormBox heading="Financial Info" />}
              {this.state.currentPageIndex == 3 && <FormBox heading="Past Performance Details" />}
              {this.state.currentPageIndex == 4 && <FormBox heading="Document Upload" />}
              <div className="nextContainer">
                <div className="btnGroup">
                  <button className="backBtn" onClick={this.previousPage.bind(this)}
                    disabled={this.state.currentPageIndex === 0 && "disabled"} >Back</button>
                  <button className="nextBtn" onClick={this.nextPage.bind(this)} disabled={this.state.currentPageIndex === 4 && "disabled"}> {this.state.currentPageIndex < 4 ? "Continue" : "Submit"}</button>
                </div>
              </div>
            </div>
          </div>
        }
      </Layout>


    )
  }
}

export default Home